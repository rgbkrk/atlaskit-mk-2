export class KeyboardEventWithKeyCode extends KeyboardEvent {
  constructor(type: string, options: any) {
    super(type, options);
  }
}
